# Directory for scripts

This directory is meant to store all the python scripts. Modules should be
placed in the python directory instead, so they can be easily shared among
scripts. When running an analysis, all the executed scripts should be
located inside this directory. Since run-time directories can not be
saturated with heavy files (like ROOT files, and often image files), it is a
good practice to configure the scripts to use an alternative directory.
Configure your package functions so an environment variable can determine
where to save/load the different files, like:

```bash
PACKAGE_FILES_PATH='/files/dir' PACKAGE_PLOTS_PATH='/plots/dir' python script.py
```

so in *script.py* one has

```python
import os

files_path = os.environ.get('PACKAGE_FILES_PATH', '/default/files/dir')
plots_path = os.environ.get('PACKAGE_PLOTS_PATH', '/default/plots/dir')
```

It is even a better idea to create a function in the package python modules
to handle these directories.
