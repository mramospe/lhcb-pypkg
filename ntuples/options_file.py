"""Example of an options file for DaVinci
"""
from Configurables import DaVinci

DaVinci().DataType = '2016'
DaVinci().EvtMax = -1
DaVinci().Simulation = True
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().PrintFreq = 5000
