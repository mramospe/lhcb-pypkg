# Contributing to LHCb PyPkg

Issues are handled through the corresponding GitLab
[issues](https://gitlab.cern.ch/mramospe/lhcb-pypkg/-/issues)
web-page. If you want to apply major modifications ask for developer access,
clone the repository and create a merge request from a new branch. If the
modification only affect the documentation please add the *documentation*
flag, so only the relevant stages of the pipeline are run.
