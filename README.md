# Template for LHCb packages based in python

[![pipeline status](https://gitlab.cern.ch/mramospe/lhcb-pypkg/badges/master/pipeline.svg)](https://gitlab.cern.ch/mramospe/lhcb-pypkg/-/commits/master)
[![coverage report](https://gitlab.cern.ch/mramospe/lhcb-pypkg/badges/master/coverage.svg)](https://gitlab.cern.ch/mramospe/lhcb-pypkg/-/commits/master)

This packages aims at serving as a template to generate packages for data
analysis with a python structure (installable with
[pip](https://pypi.org/project/pip/)).
The structure is defined in such a way that it becomes possible to write C++
extensions and load them from Python using the C-API.
As any other python package, the user must modify the name of the python
directory [lhcb_pypkg](lhcb_pypkg) and also the name of the package in
[setup.py](setup.py). The latter can also be modified in order to change
the configuration for the C++ extensions or to add data packages. The
structure of the package is the following:

* `bin`: contains executable scripts that will be included into `PATH` once
  the package is installed.
* `docs`: directory to store the note and the paper. Since in general these
  documents will be placed on a different repository, they are loaded as
  submodules.
* `lhcb_pypkg`: the name of this directory must be changed in order to suit
  the needs of the user.
* `ntuples`: directory to place lhcb/DaVinci> options files to create ROOT files for
  the analysis.
* `scripts`: directory to store python scripts.
* `src`: directory with the source files to create C++ extensions.
* `test`: directory with the scripts used to test the package functions and
  classes.
* `.clang-format`: configuration file defining the format for C++ code.
* `.gitignore`: specifies the files for git to ignore.
* `.gitlab-ci.yml`: defines the way to do the continuous integration (CI).
* `.gitmodules`: defines the submodules of the repository. By default the
  note and the paper are specified as an example.
* `CONTRIBUTING.md`: file defining the way to contribute to the package.
* `README.md`: corresponds to this file. Change it to describe your package.
* `pytest.ini`: contains the configuration of the testing suit, that is done
  using [pytest](https://docs.pytest.org/en/stable/).
* `requirements.txt`: file specifying the package dependencies of the scripts,
  which can be installed using `pip install -r requirements.txt`. One can
  include these packages in [setup.py](setup.py), although sometimes it is
  better to separate the two types of dependencies.
* `setup.cfg`: configuration of the python installation.
* `setup.py`: script to run the installation. It also contains functions to
  apply and check the format of the code in the package using
  [autopep8](https://pypi.org/project/autopep8),
  [pyflakes](https://pypi.org/project/pyflakes) and
  [clang-format](https://clang.llvm.org/docs/ClangFormat.html).

## Installation

The LHCb recommended way to install this package (and packages based on this
template) is using the existing `lb-conda-dev` commands.

```bash
lb-conda-dev virtual-env default venv
venv/run pip install --e .
```

This will install the package in the current folder using the environment
 `default` provided by `lb-conda`. This means that any modification to the python
files can be tested without needing to re-install the package. However, the `C++`
extensions need to be re-installed, if modified. Note that `lb-conda` uses `conda`
as a package manager, and the associated directories are not writeable. As long
as your project does not need any special packages with dependencies that are not
satisfied by the environment the instructions specified above will work fine. If
your package has some dependencies that are not satisfied by the environment,
a call to `pip install` might end up installing modules and scripts that are not
accessible to the `conda` packages (`conda` and `pip` are two different and
independent python package managers). This will more likely cause errors in the
future that might be difficult to debug. As a workaround, you can force `pip`
to install all the dependencies, at the cost of increasing the size of your local
installation, by adding the `--force-reinstall` flag:

```bash
venv/run pip install --force-reinstall --e .
```

This option will need to be provided for any new package that you need to install.

As an alternative, it is also possible to create a virtual environment using an `LCG`
release. The main problem of usig `LCG` and virtual environments is that the
`PYTHONPATH` is overwritten by the former, being unable to update some packages and
clashes due to scripts and modules installed in `cvmfs` and in the environment at the
same time. A workaround is proposed in the file [.vpython3](.vpython3), which creates
the environment if it does not already exist and activates it. It is recommended to
run it as:

```bash
bash --rcfile .vpython3
```

This bash script removes the `site-packages` from `LCG` so it does not appear in
the `PYTHONPATH`.

Note that with both `lb-conda` and `venv`, we can create a single virtual
environment and install several packages within it (also for different analysis).
More information about `lb-conda` can be obtained from the
[dedicated GitLab pages](https://gitlab.cern.ch/lhcb-core/lbcondawrappers).

In the following explanation, it is assumed that the package has been installed
using `lb-conda`. If using a virtual environment you just need to activate it
and remove the leading `venv/run` snippet from the commands.

## Adding scripts

Shell scripts can be added to the [bin](bin) directory. After installation,
this path will be exposed so any script in it will be visible. If we create
a script like

```bash
#!/bin/bash
echo "We rock!"
```

called `custom_script` and we put it under [bin](bin), after installation we
will be able to call

```bash
custom_script
We rock!
```

## Writing C++ extensions

Python extensions can created from C++ code using the Python C-API.
Extensions can be built either using the standard Python framework or by calling
`cmake` in addition to `make` or `ninja`.
Read [setup.py](setup.py)) for more details on how to specify the build process.

### Extensions using the Python framework

The simplest way to define an extension is to modify
[src/example_source/example_source.cpp](src/example_source/example_source.cpp)
so its name fits your needs.
In order to expose a function, first it needs to be defined either as a header
(under [src/include](src/include)) or as a combination of a header and a source
file (under [src/source](src/source)).
Then the function needs to be exposed in
[src/example_source/example_source.cpp](src/example_source/example_source.cpp)
using the python C-API.

The extension is specified in [setup.py](setup.py) as

```python
modules = [
   Module('extension1', 'extension1.cpp'),
   Module('extension2', 'extension2.cpp', root_libs=True),
   Module('extension3', 'extension3.cpp', config={'libraries': ['gsl']}),
]
```

Note that in the second case we are including the ROOT headers, and the libraries
can be handled with the `root_libs` option.
In the third case we are including the [GSL](https://www.gnu.org/software/gsl)
library.
Check [setup.py](setup.py) to see all the compilation and linking variables that
can be modified.

### Extensions with CMake

This package also shows how to include libraries built with
[CMake](https://cmake.org/) in your project.
The procedure is very similar to the previous python extension, but in
this case the libraries are built beforehand, and the API file is finally
compiled and linked to them using the Python C-API.
The structure of the directories should be something like:

```
src
└───extension
    ├───CMakeLists.txt
    ├───source
    ├───include
    └───extension.cpp
```

It is mandatory that the `CMakeLists.txt` file sets the output directory path
for the libraries as `src/lib`.

On a similar way to the previous type of extensions, these are specified in
[setup.py](setup.py) as

```python
cmake_modules = [
   CMakeModule('extension1', 'extension1.cpp'),
   CMakeModule('extension2', 'extension2.cpp', cmake_args='-G Ninja', build_command='ninja', root_libs=True),
   CMakeModule('extension3', 'extension3.cpp', config={'libraries': ['gsl']}),
]
```

In this case, however, we can specify arguments to be passed to `cmake` and also
the command used to build the extension.
In the second case, we are using `ninja`, and we are compiling against the ROOT
libraries.
Specific ROOT libraries can be used providing a list with their names.

### Handling multiple extensions

You can create different extensions by replicating the directory structure
that you see here. For instance, you could create extensions like

```
src
├───extension1
│   ├───source
│   ├───include
│   └───extension1.cpp
│
├───extension2
│   ├───source
│   ├───include
│   └───extension2.cpp
│   
└───extension3
    ├───include
    └───extension3.cpp
```

Remember to modify [setup.py](setup.py) accordingly, in such a way that
you expose all the extensions:

```python
modules = [
   Module('extension1', 'extension1.cpp'),
   Module('extension2', 'extension2.cpp', root_libs=True),
   Module('extension3', 'extension3.cpp', config={'libraries': ['gsl']}),
]
```

### Compiling against the ROOT libraries

When using an extension depending on the ROOT libraries, remember that you might
need to import ROOT before the extension to ensure that the dynamic libraries
are correctly loaded, as well as their dependencies.

## Adding data packages

Sometimes it is needed to save withing the package some configuration or
data files. These can be added by creating sub-directories inside the python
main directory.

```
lhcb_pypkg
├───__init__.py
├───mod1.py
├───mod2.py
│
└───data
    ├───configuration.json
    └───some_data.txt
```

In this case, one can set, in [setup.py](setup.py)

```python
PACKAGE_DATA = ['data/*']
```

and a directory will be created in the installation path. The path to the
data can be obtained like

```python
import os
import lhcb_pypkg

pkg_dir = os.path.abspath(os.path.dirname(lhcb_pypkg.__file__))

data_dir = os.path.join(pkg_dir, 'data')
```

since the sub-directory structure will be replicated in the installation path.

## Setting up the CI

There are a set of tools in this package that allow to handle the integration
with the GitLab CI more easily. There are two fundamental points of any CI,
which are unit tests and code format.

### Running the tests

The tests are located in the [test](test) directory. Tests are run using
[pytest](https://docs.pytest.org/en/stable). The required python packages
can be installed running

```bash
venv/run pip install -r test/requirements.txt
```

The file [pytest.ini](pytest.ini) defines the configuration for
[pytest](https://docs.pytest.org/en/stable), so in order to run all the
test functions in the directory you simply need to run

```bash
venv/run pytest -v
```

### Applying and checking the format

The python format is handled by [autopep8](https://pypi.org/project/autopep8),
whilst the C++ format is controlled by
[clang-format](https://clang.llvm.org/docs/ClangFormat.html). In order
to apply or check the format of the code, one just needs to run

```bash
venv/run python setup.py apply_format --regex "(lhcb_pypkg|src|tests|scripts|ntuples)"
venv/run python setup.py check_format --regex "(lhcb_pypkg|src|tests|scripts|ntuples)"
```

to apply and check the format of the files. This custom commands allow to apply
and check the format in a given set of directories specified through a regular
expression. It is quite common for users to check the misbehavior of a python
script at runtime. However, the [pyflakes](https://pypi.org/project/pyflakes)
module allows to detect unused modules, undefined or unused variables, ...
without running the scripts. This check can be done running

```bash
venv/run python setup.py check_pyflakes --regex "(lhcb_pypkg|src|tests|scripts|ntuples)"
```

This will display the directories where errors where found. In order to
determine the actual source of the error you will need to run, for example

```bash
venv/run pyflakes $(find . -type f -name "*.py")
```
