"""Test ROOT dependent functions
"""
import ROOT  # must load ROOT first for correct dynamic library linking
ROOT.gROOT.IgnoreCommandLineOptions = True  # ROOT is intrusive


def test_get_tree_name():
    """Test the call to "get_tree_name"
    """
    from lhcb_pypkg.cpython import example_source, example_cmake
    for ext in example_source, example_cmake:
        t = ROOT.TTree('t', 't')
        assert t.GetName() == ext.get_tree_name(t)
