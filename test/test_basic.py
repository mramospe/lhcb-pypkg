"""Test the basic package functions
"""


def _get_extension_modules():
    """Function returning the C++ extension modules
    """
    from lhcb_pypkg.cpython import example_source, example_cmake
    return example_source, example_cmake


def test_main():
    """Test that we can load the package
    """
    import lhcb_pypkg
    lhcb_pypkg.__file__  # so PyFlakes does not complain about an unused module


def test_cpython():
    """Test that we can load the extension module
    """
    _get_extension_modules()


def test_hello_world():
    """Test the call to "hello_world"
    """
    for ext in _get_extension_modules():
        ext.hello_world()


def test_say_this():
    """Test the call to "say_this"
    """
    for ext in _get_extension_modules():
        ext.say_this("We rock!")


def test_sum_integers():
    """Test the call to "sum_integers"
    """
    for ext in _get_extension_modules():
        assert ext.sum_integers(-1, 1) == 0
        assert ext.sum_integers(10, 5) == 15


def test_sum_integers_tuple():
    """Test the call to "sum_integers_tuple"
    """
    for ext in _get_extension_modules():
        assert ext.sum_integers_tuple(-1, 1, (-1, 1)) == 0
        assert ext.sum_integers_tuple(10, 5, (-4, 1)) == 12
