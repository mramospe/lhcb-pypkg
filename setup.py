"""Setup script for the package
"""
import collections
import importlib
import inspect
import os
import re
import subprocess
import warnings
from setuptools import setup, Extension, Command
from setuptools.command.build_ext import _build_ext
import sys

################################################################################
# Version of lcg-clang-format command to use
CLANG_FORMAT = 'clang-format'
################################################################################

################################################################################
# Change this to the name of your package. Note that the directory containing
# the python modules must not contain dashes "-", so replace them by
# underscores "_".
PKG_NAME = 'lhcb-pypkg'
################################################################################

# Path to the current directory
PWD = os.path.dirname(os.path.abspath(__file__))

# Path to the directory storing the scripts
SCRIPTS_PATH = os.path.join(PWD, 'bin')

# Sanitize the package name replacing dashes by underscores
SANITIZED_PKG_NAME = PKG_NAME.replace('-', '_')


class CExtension(Extension):

    def __init__(self, path, api_filename, root_libs=None, **kwargs):
        """Initialize a standard extension.
        """
        api_name, _ = os.path.splitext(api_filename)
        # actual name of the module
        pkg_name = '.'.join(
            [SANITIZED_PKG_NAME, 'cpython', os.path.basename(api_name)])

        super().__init__(pkg_name, [os.path.join(
            PWD, 'src', path, api_filename)], **kwargs)

        self.path = path
        self.root_libs = root_libs

    def prebuild_actions(self):
        """Build a CExtension or a pre-processed CMakeExtension
        """
        self.include_dirs.append(os.path.join(
            PWD, 'src', self.path, 'include'))

        source_path = os.path.join(PWD, 'src', self.path, 'source')
        if os.path.exists(source_path):
            self.sources += get_source_files(source_path, '.c', '.cpp', '.C')

        if self.root_libs:
            incdir, libdir, flags = get_root_config()
            self.include_dirs += incdir
            self.library_dirs += libdir
            self.extra_compile_args += flags
            if self.root_libs is not True:
                rlibs = set(self.root_libs)
            else:
                rlibs = set()
            rlibs.add('ROOTTPython')  # needed after ROOT 6.22
            self.libraries += list(rlibs)


class CMakeExtension(CExtension):

    def __init__(self, path, api_filename, cmake_args='', build_command='make', root_libs=None, **kwargs):
        """Initialize a extension to be built using CMake.
        """
        super().__init__(path, api_filename, root_libs, **kwargs)
        self.cmake_args = cmake_args
        self.build_command = build_command

    def prebuild_actions(self):
        """Run CMake and extend the include and library directories
        """
        cwd = os.getcwd()

        try:
            os.chdir(os.path.join(cwd, 'src', self.path))
            rc = subprocess.call(['cmake', '.'] + self.cmake_args.split())
            if rc < 0:
                raise RuntimeError(
                    f'Call to cmake exited with error code {abs(rc)}')
            rc = subprocess.call([self.build_command])
            if rc < 0:
                raise RuntimeError(
                    f'Call to {self.build_command} exited with error code {abs(rc)}')
        except:
            raise
        finally:
            os.chdir(cwd)

        self.include_dirs += self.include_dirs + \
            [os.path.join(cwd, 'src', self.path, 'include')]
        self.library_dirs += self.library_dirs + \
            [os.path.join(cwd, 'src', self.path, 'lib')]

        super().prebuild_actions()


################################################################################
# Data files can be added to the package as long as they are placed within
# the python files, that is, using a structure like
#
# lhcb_pypkg
# ├───__init__.py
# ├───mod1.py
# ├───mod2.py
# │
# └───data
#     ├───configuration.json
#     └───some_data.txt
#
# In this case, one can set
#
# PACKAGE_DATA = ['data/*']
#
# and a directory will be created in the installation path. The path to the
# data can be obtained by calling
#
# import os
# import lhcb_pypkg
#
# pkg_dir = os.path.abspath(os.path.dirname(lhcb_pypkg.__file__))
#
# data_dir = os.path.join(pkg_dir, 'data')
#
# Note that it is possible to specify multiple directories and files.
#
PACKAGE_DATA = []
################################################################################

################################################################################
# Define the C++ extension modules to export. Arguments must be:
#
# * path: relative path to the extension root (starting at src).
# * api_file: path to the file inside "path" that exposes the functions.
# * config: possible configuration for the setuptools.Extension class. If
#   provided, it must be a dictionary accepting the following keys:
#   - include_dirs: additional directories to look for headers.
#   - library_dirs: additional directories to look for libraries.
#   - libraries: names of the libraries to use.
#   - extra_compile_args: additional arguments to the C++ compiler.
#   - define_macros: list of macros to define, each one as a 2-tuple (name,
#     value).
#   - undef_macros: list of macros to undefine explicitly.
# * root_libs: possible list of ROOT libraries to link against. Setting this
#   option will modify also the values of the previous parameters according to
#   the ROOT installation options. If the value is set to an empty list, the
#   path to the include directory of ROOT will still be added.
#
modules = [
    CExtension('example_source', 'example_source.cpp', root_libs=True),
]
################################################################################

################################################################################
# If using CMake-based projects, we must compile them before we proceed. The
# arguments to each CMake module are:
#
# * path: relative path to the extension root (starting at src).
# * api_file: path to the file inside "path" that exposes the functions.
#
#
cmake_modules = [
    CMakeExtension('example_cmake', 'example_cmake.cpp', root_libs=True),
]
################################################################################


def get_source_files(path, *extensions):
    """Find files in the given path satisfying the given extensions
    """
    all_files = []
    for root, _, files in os.walk(path):
        all_files += [os.path.join(root, f) for f in filter(
            lambda s: any(s.endswith(e) for e in extensions), files)]
    return all_files


def get_root_config():
    """Get the ROOT flags. This must go to the
    """
    include_dir = subprocess.check_output(
        ['root-config', '--incdir']).decode('ascii').split()
    lib_dir = subprocess.check_output(
        ['root-config', '--libdir']).decode('ascii').split()
    flags = subprocess.check_output(
        ['root-config', '--auxcflags']).decode('ascii').split()
    return include_dir, lib_dir, flags


class build_ext(_build_ext):  # must be named "build_ext"

    def run(self):
        """Build the extensions
        """
        for e in self.extensions:
            e.prebuild_actions()
        super().run()


def check_format_process(name, directory, process, compare=None):
    """
    Check the return code of a process. If "compare" is given, it is used to
    compare the output with it. By default it just checks that there is no output.
    """
    stdout, stderr = process.communicate()

    if process.returncode < 0:
        raise RuntimeError(
            f'Call to {name} exited with error {abs(process.returncode)}; the message is:\n{stderr}')

    if compare is None:
        if len(stdout):
            raise RuntimeError(
                f'Found problems for files in directory "{directory}"')
    else:
        stdout = stdout.decode('ascii')
        if stdout != compare:
            raise RuntimeError(
                f'Found problems for files in directory "{directory}":{os.linesep}'
                f'<<< IN:{os.linesep}{stdout}\n'
                f'>>> REF:{os.linesep}{compare}')


class DirectoryWorker(Command):

    user_options = [
        ('regex=', 'r', 'regular expression defining the directories to process'),
    ]

    def initialize_options(self):
        """Running at the begining of the configuration.
        """
        self.regex = None
        self.directories = None

    def finalize_options(self):
        """Running at the end of the configuration.
        """
        if self.regex is None:
            raise Exception('Parameter --regex is missing')

        m = re.compile(self.regex)

        self.directories = list(os.path.join(PWD, d) for d in filter(
            lambda s: os.path.isdir(s) and (m.match(s) is not None), os.listdir(PWD)))

        if len(self.directories) == 0:
            warnings.warn('Empty list of directories', RuntimeWarning)


class ApplyFormatCommand(DirectoryWorker):

    description = 'apply the format to the files in the given directories'

    def run(self):
        """Execution of the command action.
        """
        for directory in self.directories:

            # Format the python files
            python_files = get_source_files(directory, '.py')
            python_proc = None if not python_files else subprocess.Popen(
                ['autopep8', '-i'] + python_files)

            # Format C files
            c_files = get_source_files(
                directory, '.c', '.cpp', '.C') + get_source_files(directory, '.h', '.hpp')
            c_proc = None if not c_files else subprocess.Popen(
                [CLANG_FORMAT, '-i'] + c_files)

            def killall():
                return all(p.kill() for p in (python_proc, c_proc) if p is not None)

            # Wait for the processes to finish
            if python_proc is not None and python_proc.wait() != 0:
                killall()
                raise RuntimeError(
                    'Problems found while formatting python files')

            if c_proc is not None and c_proc.wait() != 0:
                killall()
                raise RuntimeError('Problems found while formatting C files')


class CheckFormatCommand(DirectoryWorker):

    description = 'check the format of the files of a certain type in a given directory'

    def run(self):
        """Execution of the command action.
        """
        for directory in self.directories:
            python_files = get_source_files(directory, '.py')
            c_files = get_source_files(
                directory, '.c', '.cpp', '.C') + get_source_files(directory, '.h', '.hpp')

            # Check python files
            if python_files:
                process = subprocess.Popen(['autopep8', '--diff'] + python_files,
                                           stdout=subprocess.PIPE,
                                           stderr=subprocess.PIPE)
                check_format_process('autopep8', directory, process)

            # Check the C files
            for fl in c_files:
                process = subprocess.Popen([CLANG_FORMAT, fl],
                                           stdout=subprocess.PIPE,
                                           stderr=subprocess.PIPE)
                with open(fl) as f:
                    check_format_process(
                        CLANG_FORMAT, directory, process, compare=f.read())


class CheckPyFlakesCommand(DirectoryWorker):

    description = 'run pyflakes in order to detect unused objects and errors in the code'

    def run(self):
        """Execution of the command action.
        """
        for directory in self.directories:

            python_files = get_source_files(directory, '.py')

            if python_files:

                process = subprocess.Popen(['pyflakes'] + python_files,
                                           stdout=subprocess.PIPE,
                                           stderr=subprocess.PIPE)

                try:
                    check_format_process('pyflakes', directory, process)
                except RuntimeError:
                    files = os.linesep.join(python_files)
                    raise RuntimeError(
                        f'PyFlakes failed to process files:{os.linesep}{files}')


class CheckDocumentationCommand(Command):

    description = 'check that all the exposed functions have documentation about them and their arguments'

    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def _get_missing_descriptions(self, obj, from_class=False):
        """
        Get the names of the arguments that are not documented in the provided
        function. If *from_class* is provided, then the object is assumed to
        be a class member.
        """
        s = inspect.getfullargspec(obj)

        missing = []
        if s.args is not None and obj.__doc__:

            if from_class:
                args = s.args[1:]  # first argument is the class
            else:
                args = s.args

            for a in filter(lambda s: not f':param {s}:' in obj.__doc__, args):
                missing.append(a)

        return missing

    def run(self):
        """Execution of the command action.
        """
        module = importlib.import_module(
            'minkit')  # import the package even if it is not installed

        missing_args = {}

        for m in module.__all__:  # iterate over exposed objects

            o = getattr(module, m)

            if inspect.isfunction(o):  # check functions
                missing = self._get_missing_descriptions(o)
                if missing:
                    missing_args[m] = missing

            elif inspect.isclass(o):  # check classes

                for n, f in inspect.getmembers(o, predicate=inspect.isfunction):
                    missing = self._get_missing_descriptions(
                        f, from_class=True)
                    if missing:
                        missing_args[f'{o.__name__}.{n}'] = missing

                for n, f in inspect.getmembers(o, predicate=inspect.ismethod):
                    missing = self._get_missing_descriptions(
                        f, from_class=True)
                    if missing:
                        missing_args[f'{o.__name__}.{n}'] = missing

        if missing_args:
            output = os.linesep.join(
                f'- {m}: {s}' for m, s in sorted(missing_args.items()))
            raise RuntimeError(
                f'Missing descriptions in the following functions:{os.linesep}{output}')


# Setup function
setup(name=PKG_NAME,
      description='This is a demo package',
      long_description='''
This is really just a demo package.
''',
      python_requires='>=3.6',
      install_requires=[],  # dependencies on python packages should be placed here
      tests_require=['pytest', 'pytest-runner', 'pytest-cov',
                      'pyflakes', 'clang-format', 'autopep8'],
      packages=[SANITIZED_PKG_NAME],
      package_data={SANITIZED_PKG_NAME: PACKAGE_DATA},
      ext_modules=modules + cmake_modules,
      scripts=list(os.path.join(SCRIPTS_PATH, s)
                   for s in os.listdir(SCRIPTS_PATH) if s != 'README.md'),
      cmdclass={'apply_format': ApplyFormatCommand,
                'check_format': CheckFormatCommand,
                'check_pyflakes': CheckPyFlakesCommand,
                'check_documentation': CheckDocumentationCommand,
                'build_ext': build_ext,
                })
