// C-API
#include <Python.h>

// ROOT Python C-API (only needed if the extension uses ROOT)
#include "TPython.h"

// LOCAL
#include "basic.hpp"
#include "root.hpp"

static PyObject *hello_world(PyObject *self, PyObject *args) {
  if (!PyArg_ParseTuple(args, ""))
    return NULL;

  basic::hello_world();

  return Py_BuildValue("");
}

static PyObject *say_this(PyObject *self, PyObject *args) {
  char const *s = nullptr;

  if (!PyArg_ParseTuple(args, "s", &s))
    return NULL;

  basic::say_this(s);

  return Py_BuildValue("");
}

static PyObject *sum_integers(PyObject *self, PyObject *args) {

  int a, b;

  if (!PyArg_ParseTuple(args, "ii", &a, &b))
    return NULL;

  return Py_BuildValue("i", basic::sum_integers(a, b));
}

static PyObject *sum_integers_tuple(PyObject *self, PyObject *args) {

  int a, b, c, d;

  if (!PyArg_ParseTuple(args, "ii(ii)", &a, &b, &c, &d))
    return NULL;

  return Py_BuildValue("i", basic::sum_integers(a, b, c, d));
}

static PyObject *get_tree_name(PyObject *self, PyObject *args) {

  PyObject *o;

  if (!PyArg_ParseTuple(args, "O", &o))
    return NULL;

  TTree *tree = static_cast<TTree *>(TPython::CPPInstance_AsVoidPtr(o));

  return Py_BuildValue("s", root::get_tree_name(tree));
}

// Functions to export
static PyMethodDef Methods[] = {
    {"hello_world", hello_world, METH_VARARGS, "Display 'Hello world!'"},
    {"say_this", say_this, METH_VARARGS, "Display the input argument"},
    {"sum_integers", sum_integers, METH_VARARGS, "Sum two integers"},
    {"sum_integers_tuple", sum_integers_tuple, METH_VARARGS,
     "Sum integers with a tuple"},
    {"get_tree_name", get_tree_name, METH_VARARGS,
     "Get the name of a TTree object"},
    {NULL, NULL, 0, NULL}};

// Define the module
static struct PyModuleDef example_source = {
    PyModuleDef_HEAD_INIT,
    "example_source",                             // name of the module
    "Example of C++ functions exposed to Python", // documentation
    -1,      // size of per-interpreter state of the module, or -1 if the module
             // keeps state in global variables
    Methods, // methods being exposed
};

// Initialize the module
PyMODINIT_FUNC PyInit_example_source(void) {

  return PyModule_Create(&example_source);
}
