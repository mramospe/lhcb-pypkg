#include "TTree.h"

namespace root {

  /// Get the name of a TTree object
  char const *get_tree_name(TTree *tree) {

    if (tree)
      return tree->GetName();
    else
      return nullptr;
  }
} // namespace root
