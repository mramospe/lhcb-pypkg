namespace basic {
  /// The first function in computing history ;)
  void hello_world();

  /// Similar to "hello_world", but says whatever we want
  void say_this(char const *);

  /// Sum some integers
  int sum_integers(int, int);

  /// Sum some integers
  int sum_integers(int, int, int, int);
} // namespace basic
