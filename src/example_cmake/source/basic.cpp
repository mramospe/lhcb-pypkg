#include <iostream>

namespace basic {

  void hello_world() { std::cout << "Hello world!" << std::endl; }

  void say_this(char const *s = nullptr) {
    if (s)
      std::cout << s << std::endl;
  }

  int sum_integers(int a, int b) { return a + b; }

  int sum_integers(int a, int b, int c, int d) { return a + b + c + d; }
} // namespace basic
